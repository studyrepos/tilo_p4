% TiLo 2020 - P4 - Ex1 - NEA

% NEA for MevenAB
% A valid word has the same number of a and b

state(start).                               % define valid states
state(sAoBo).
state(sAeBo).
state(sAoBe).
state(sAeBe).

sigma(b).                                   % define alphabet
sigma(a).

start(start).                               % define start state
end(sAeBe).                                 % define end state

delta(start, a, sAoBe).                     % define transitions
delta(start, b, sAeBo).
delta(sAoBo, a, sAeBo).
delta(sAoBo, b, sAoBe).
delta(sAoBe, a, sAeBe).
delta(sAoBe, b, sAoBo).
delta(sAeBo, a, sAoBo).
delta(sAeBo, b, sAeBe).
delta(sAeBe, a, sAoBe).
delta(sAeBe, b, sAeBo).

% tutorium
sigma_stern([]).                            % base for empty word
sigma_stern([X|Xs]) :-                      %
    sigma(X), sigma_stern(Xs).              % recursive split for non empty words

delta_stern(Zact, [], Zact).                % base for empty transition
delta_stern(Zact, [W|Ws], Znew) :-          % non empty transition
    state(Zact),                            % actual state has to be defined
    state(Znew),                            % new state has to be defined
    delta(Zact, W, Z),                      % calculate delta for actual symbol
    delta_stern(Z, Ws, Znew).               % recursive call from state after actual symbol to remaining symbols


lvonN(Ws) :-                                % calculate language
    sigma_stern(Ws),                        % all words in N
    start(S),                               % a valid start state
    end(E),                                 % a valid end state
    delta_stern(S, Ws, E).                  % start, end and words have to validate for transitions


% testing
automat(S, []) :-                           % call with empty symbols list
    end(S).                                 % state S has to be a valid end state
automat(S, [X|Xs]) :-                       % call with non empty symbol list
    sigma(X),                               % test for first symbol X part of the alphabet
    state(S),                               % S has to be a valid state
    delta(S, X, T),                         % calculate transition for X on S, save temp state as T
    automat(T, Xs).                         % recursive call on temp state and remaining symbols
